import React, { Component } from 'react';
import {Layout, Header, Content} from 'react-mdl';
import WeightInput from './WeightInput';
import WeightGraph from './WeightGraph';
import './App.css';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			weightLog: [
				{date: "Tue Apr 25 2017 17:51:59 GMT+0100 (GMT Daylight Time)", value: 83.3},
				{date: "Wed Apr 26 2017 17:51:59 GMT+0100 (GMT Daylight Time)", value: 82.4},
				{date: "Thu Apr 27 2017 17:51:59 GMT+0100 (GMT Daylight Time)", value: 82.7},
				{date: "Fri Apr 28 2017 17:51:59 GMT+0100 (GMT Daylight Time)", value: 82.2},
				{date: "Sat Apr 29 2017 17:51:59 GMT+0100 (GMT Daylight Time)", value: 81.7},
				{date: "Sun Apr 30 2017 17:51:59 GMT+0100 (GMT Daylight Time)", value: 81.9},
				{date: "Mon May 01 2017 17:51:59 GMT+0100 (GMT Daylight Time)", value: 82.1},
				{date: "Tue May 02 2017 17:51:59 GMT+0100 (GMT Daylight Time)", value: 82.6},
				{date: "Wed May 03 2017 17:51:59 GMT+0100 (GMT Daylight Time)", value: 83.3}
			]
		}

		this.saveWeight = this.saveWeight.bind(this)
	}

	saveWeight(weight) {
		let weights = this.state.weightLog;
		let newWeight = {date: new Date(), value: weight};
		weights.push(newWeight);

		this.setState({weightLog: weights});
	}

	render() {
		return (
			<Layout fixedHeader>
				<Header title="Log Weight" />
				<Content>
					<WeightInput
						lastWeight={this.state.weightLog[this.state.weightLog.length-1].value}
						saveWeight={this.saveWeight}
					/>
					<WeightGraph weightLog={this.state.weightLog} />
				</Content>
			</Layout>
		);
	}
}

export default App;