import React, { Component } from 'react';
import {Card, CardTitle, CardText} from 'react-mdl';

export default class WeightGraph extends Component {

	constructor(props) {
		super(props);
		this.state = {
			difference: 0
		}
	}

	componentDidMount() {
		this.getAverage();
		this.drawGraph();
	}

	componentWillReceiveProps() {
		this.getAverage();
		this.drawGraph();
	}

	drawGraph() {
		let canvas = document.getElementById('canvas');
		if(canvas.getContext) {

			let max = this.getMax();
			let min = this.getMin();
			this.setState({max: max, min: min});

			let l = this.props.weightLog.length;
			let y = 0;
			let yIncrement = this.refs.graph.offsetWidth/6;

			let ctx = canvas.getContext('2d');
			ctx.fillStyle = "white";
			ctx.fillRect(0,0,canvas.width,canvas.height);
			ctx.beginPath();
			ctx.moveTo(0, this.normaliseX(this.props.weightLog[l-7].value, max, min));
			for(let i = l-6; i < l; i++) {
				ctx.lineTo(y += yIncrement, this.normaliseX(this.props.weightLog[i].value, max, min));
			}
			ctx.stroke();
		}
	}

	getMax() {
		let l = this.props.weightLog.length;
		let max = this.props.weightLog[l-7].value;
		for(let i = l-6; i < l; i++) {
			if(this.props.weightLog[i].value > max) {
				max = this.props.weightLog[i].value;
			}
		}
		return max;
	}

	getMin() {
		let l = this.props.weightLog.length;
		let min = this.props.weightLog[l-7].value;
		for(let i = l-6; i < l; i++) {
			if(this.props.weightLog[i].value < min) {
				min = this.props.weightLog[i].value;
			}
		}
		return min;
	}

	normaliseX(x, max, min) {
		return this.refs.graph.offsetHeight - (x - min) / (max - min) * this.refs.graph.offsetHeight;
	}

	getAverage() {
		let l = this.props.weightLog.length;
		let sum = 0;
		for(let i = l-7; i < l; i++) {
			let val = this.props.weightLog[i].value;
			sum += val;
		}
		let ave = +(sum/7).toFixed(1);
		if(this.state.average > 0) {
			let diff = +(ave - this.state.average).toFixed(1);
			console.log(ave, diff);
			if(diff > 0) { diff = "+" + diff }
			this.setState({difference: diff});
		}
		this.setState({average: ave});
	}

	render() {
		return (
			<Card shadow={0} style={{margin: 'auto'}}>
					<CardTitle style={{color: '#fff', background: '#729'}}>Weight History</CardTitle>
					<CardText>
						<canvas id="canvas" ref="graph"></canvas>
						<h5>7 Days</h5>
						<ul>
							<li>Average: {this.state.average} (<span className={this.state.difference <= 0 ? "green" : "red"}>{this.state.difference}</span>)</li>
							<li>Min: {this.state.min}</li>
							<li>Max: {this.state.max}</li>
						</ul>
					</CardText>
			</Card>
		)
	}
}