import React, { Component } from 'react';
import {Card, CardTitle, CardText, CardActions, Button, Textfield} from 'react-mdl';

export default class WeightInput extends Component {
	constructor(props) {
		super(props);
		this.state = {
			weightToday: this.props.lastWeight
		};

		this.increaseWeight = this.increaseWeight.bind(this);
		this.decreaseWeight = this.decreaseWeight.bind(this);
		this.saveWeight = this.saveWeight.bind(this);
	}

	increaseWeight(e) {
		e.preventDefault();
		this.setState((prevState) => {
			return {weightToday: +(prevState.weightToday + 0.1).toFixed(2)}
		});
	}

	decreaseWeight(e) {
		e.preventDefault();
		this.setState((prevState) => {
			return {weightToday: +(prevState.weightToday - 0.1).toFixed(2)}
		});
	}

	saveWeight(e) {
		e.preventDefault();
		this.props.saveWeight(this.state.weightToday);
	}

	render() {
		return (
			<Card shadow={0} style={{margin: 'auto'}}>
					<CardTitle expand style={{color: '#fff', background: '#729'}}>Today's Weight</CardTitle>
					<CardText>
						<Textfield
							onChange={() => {}}
							pattern="-?[0-9]*(\.[0-9]+)?"
							error="Input is not a number!"
							label="Weight"
							floatingLabel
							value={this.state.weightToday}
						/>
						<Button onClick={this.decreaseWeight}>-</Button>
						<Button onClick={this.increaseWeight}>+</Button>
					</CardText>
					<CardActions border>
						<Button className="weight-input-save" colored ripple raised onClick={this.saveWeight}>Save</Button>
					</CardActions>
			</Card>
		)
	}
}